NetVis
======

Visualizations for anonymity-CTF network traces.

Website
-------

The visualization code is mainly in [teamgraph.html](teamgraph.html) and [js/netvis.js](teamgraph.html). This site expects to have an paramaeter for the teamId.
For team `64000` the url would be [teamgraph.html#64000](teamgraph.html#64000). The Site will try to load the preprocessed trace from [tmp/teamgraph-64000.json].


### Data

The data for the Graphs is stored in one json per team. The json consists of one object with the following keys:

- `meta`: stores the challenge node
- `nodes`: all nodes and their positions and weights
- `egdes`: all edges and their weights. (the available weights are also stored in the `meta.json`)
- `timeline`: a compressed version of all ticks
- `attacks`:
  - `candidates`: final candidates for senderset attack
  - `raw_candidates`: possible senders but without intersecting all challenge messages
  - `analysis`: Data for statistics
  - `comgraph`: communication tree for senderset attack
  - `challenge_receiver`: some experiment but didn't work


### NetGraph

The netgraph shows the undelay network. By default the nodesize and brightness show the traffic volume comulated over all ticks.

The weight can be selected by setting the `selected_weight` cookie. The selected weight is stored in a cookie so the data can be shared between iframes. An example for setting the cookie can be found in [index_teams.html](index_teams.html).


Preprocessing
-------------

The traces need some preprocessing before the can be send to the browser. This will mainly compress, filter and precalculate the trace. Multiple traces can be processed at once and the expensive operations will be cached.

Processing 2 traces (`traces/01.json`, `traces/03.json`) with the network in `graph/graph_1000_1_0_15.pickle` and saving the result in `tmp`:

```
./netvis_tools.py --network graph/graph_1000_1_0_15.pickle gen-all --trace traces/01.json --trace traces/03.json --out-dir tmp
```
