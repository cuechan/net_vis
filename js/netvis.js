const NODE_SIZE_FACTOR = 1;
const MIN_COL = "#171717";
const MAX_COL = "#FFFFFF";
const COLORWHEEL_STEPS = 250;

// create an array with nodes
var graph, timeline, meta, network;
let all_nodes, all_edges, vis_nodes, vis_edges;
const SCALE = 3000;
var slider_playing = false;
var slider_mover_id = null;
var manipulated_edges = [];
var teams = [];
var challenge_node_visible = false;
var senderset = [];



async function get_meta_information() {
    return fetch("tmp/meta.json").then((meta) => {
        return meta.json();
    })
}


function load_team_network(network_area, teamname) {
    console.log("loading graph for " + teamname);

    fetch("tmp/teamgraph-" + teamname + ".json")
        .then((data) => data.json())
        .then((team_graph_data) => {
            let nodes = team_graph_data.nodes;
            let edges = team_graph_data.edges;
            meta = team_graph_data.meta;
            senderset = team_graph_data.attacks.candidates;

            // graph = g;
            // console.log(nodes_list );
            let max_node_weight = 0;

            nodes.forEach(n => {
                // console.log(n);
                max_node_weight = Math.max(max_node_weight, n.weight.packets_total);
            });

            // color palette generator
            let base_color = new KolorWheel(MIN_COL);
            var target_color = base_color.abs(MAX_COL, COLORWHEEL_STEPS);

            console.log("max weight:", max_node_weight);

            all_nodes = nodes.map(n => {
                let weight = n.weight.packets_total;
                let relative_weight = weight / max_node_weight;

                var x = {
                    id: n.id,
                    x: n.x * SCALE,
                    y: n.y * SCALE,
                    label: n.id.toString(),
                    value: weight,
                    color: target_color.get(parseInt(relative_weight * (COLORWHEEL_STEPS - 1))).getHex(),
                    weights: n.weight,
                };
                return x;
            });

            // this is very unecessary
            all_edges = edges.map(e => {
                var x = {
                    id: e.from.toString() + "-" + e.to.toString(),
                    from: e.from,
                    to: e.to,
                    value: e.value,
                    // color: target.get(parseInt(e.value * (COLORWHEEL_STEPS -1 ))).getHex(),
                    // color: target.get(4).getHex(),
                };
                return x;
            });

            console.log(all_nodes.length + " nodes");
            console.log(all_edges.length + " edges");

            vis_nodes = new vis.DataSet(all_nodes);
            vis_edges = new vis.DataSet(all_edges);


            // create a network
            var data = {
                nodes: vis_nodes,
                edges: vis_edges,
            };
            var options = {
                edges: {
                    width: 1,
                    color: "rgb(80,80,80)",
                },
                nodes: {
                    shape: "dot",
                    font: { color: "lime" }
                    // shapeProperties: { interpolation: false},
                },
                layout: {
                    improvedLayout: false
                },
                physics: false,
                interaction: {
                    dragNodes: false
                },
            };

            network = new vis.Network(network_area, data, options);

            var cookie_change_checker = function () {
                var lastCookie = document.cookie;
                return function () {
                    // console.log("check for changed cookies");

                    var currentCookie = document.cookie;
                    if (currentCookie != lastCookie) {
                        lastCookie = currentCookie;
                        weight_selection_changed();
                    }
                };
            }();

            window.setInterval(cookie_change_checker, 100); // run every 100 ms

            labels = []
            for (let index = 0; index < team_graph_data.attacks.analysis.length; index++) {
                labels.push(index)
            }

            const message_activity_data = {
                labels: labels,
                datasets: [{
                    label: 'Message Activity',
                    backgroundColor: 'rgb(255, 99, 132)',
                    borderColor: 'rgb(255, 99, 132)',
                    data: team_graph_data.attacks.analysis.map((n) => n.message_activity)
                }]
            };

            const sender_set_size_data = {
                labels: labels,
                datasets: [{
                    label: 'Senders set size',
                    backgroundColor: 'rgb(255, 99, 132)',
                    borderColor: '#ff00e1',
                    data: team_graph_data.attacks.analysis.map((n) => n.sender_set_size)
                }]
            };

            const chart_options = {
                legend: { display: false },
                responsive: true,
                elements: {
                    point: {
                        radius: 0
                    }
                }
            };


            const config_activity_chart = {
                type: 'line',
                data: message_activity_data,
                options: chart_options,
            };

            const config_senders_set_chart = {
                type: 'line',
                data: sender_set_size_data,
                options: chart_options,
            };



            new Chart(
                document.getElementById('team_network_activity'),
                config_activity_chart
            );

            new Chart(
                document.getElementById('team_network_sender_set_size'),
                config_senders_set_chart
            );

            let area = document.getElementById("challenge_communications");
            create_challenge_communications_graph(area, team_graph_data.attacks.comgraph);
        });
}


function create_challenge_communications_graph(networkarea, graphdata) {
    // challenge_communications
    vis_nodes_challenge = new vis.DataSet(graphdata.nodes);
    vis_edges_challenge = new vis.DataSet(graphdata.edges);

    console.log(graphdata);

    // create a network
    let data = {
        nodes: vis_nodes_challenge,
        edges: vis_edges_challenge,
    };
    let options = {
        // edges: {
        //     width: 1,
        //     color: "rgb(80,80,80)",
        // },
        nodes: {
            shape: "dot",
            // shapeProperties: { interpolation: false},
            font: {color: "lime"}
        },
        layout: {
            improvedLayout: false,
            // hierarchical: {
            //     direction: "UD",
            // },
        },
        physics: true,
        interaction: {
            dragNodes: true
        },
    };

    network2 = new vis.Network(networkarea, data, options);
    network2.on("selectNode", function (params) {
        var moveToOptions = {
            // position: { x: x, y: x },
            scale: 1.0,
            // offset: { x: x, y: y },
            animation: {
                duration: 200,
                easingFunction: "easeInOutQuad"
            }
        };

        console.log("selectNode Event:", params);
        network.focus(params.nodes[0], moveToOptions);
    });
}





function weight_selection_changed() {
    let selection = getCookie("selected_weight");
    console.log(selection);
    update_weights(selection);
}



function weight_selection_onclick(selection) {
    console.log("change weight selection to " + selection.value);
    setCookie("selected_weight", selection.value);
}


function show_senderset(button) {
    senderset.forEach(n => {
        vis_nodes.updateOnly({
            id: n,
            shape: "triangle",
            borderWidth: 1,
            color: {
                background: "red",
                highlight: { background: "red", border: "#00FFF4" },
            },
        });
    })
}




function update_weights(weight) {
    let base_color = new KolorWheel(MIN_COL);
    let target_color = base_color.abs(MAX_COL, COLORWHEEL_STEPS);
    let max_something = 0;

    for (let index = 0; index < all_nodes.length; index++) {
        max_something = Math.max(max_something, all_nodes[index].weights[weight]);
    }

    for (let index = 0; index < all_nodes.length; index++) {
        let new_val = all_nodes[index].weights[weight];
        let new_relative_val = (new_val / max_something);

        if (new_relative_val > 1) {
            console.log("aAAAHHHHHhhhhhhhh");
        }

        all_nodes[index].value = new_val;
        all_nodes[index].color = target_color.get(parseInt(new_relative_val * (COLORWHEEL_STEPS - 1))).getHex();

        // node.color = "#ffff00";

        // console.log(node.id);
        // vis_nodes.update(node);

        // var x = {
        //     id: n.id,
        //     x: n.x * SCALE,
        //     y: n.y * SCALE,
        //     label: n.id.toString(),
        //     value: 1,
        //     color: "#ffff00",
        //     // color: target_color.get(parseInt(relative_weight * (COLORWHEEL_STEPS - 1))).getHex(),
        //     weights: n.weight,
        // };
    }

    // let new_nodes = new vis.DataSet(all_nodes);

    // vis_nodes.clear();
    vis_nodes.update(all_nodes);

    // console.log(vis_nodes);

}






function slider_on_input(slider) {
    slider_playing = false;
    updateGraph(slider);
}

function show_challenge_node(button) {
    console.log(meta);
    if (challenge_node_visible) {
        challenge_node_visible = false;
        vis_nodes.updateOnly({
            id: meta.challenge_node,
            shape: "circle",
            border: false,
            borderWidth: false,
        });
        weight_selection_changed();
        button.innerHTML = "Show challenge node";
    }
    else {
        challenge_node_visible = true;
        vis_nodes.updateOnly({
            id: meta.challenge_node,
            shape: "triangle",
            borderWidth: 5,
            color: {
                highlight: { background: "red", border: "#00FFF4" },
            },

            // color: "#FF00E0",
        });

        button.innerHTML = "Hide challenge node";
        var moveToOptions = {
            // position: { x: x, y: x },
            scale: 1.0,
            // offset: { x: x, y: y },
            animation: {
                duration: 200,
                easingFunction: "easeInOutQuad"
            }
        };

        network.focus(meta.challenge_node, moveToOptions);
        network.selectNodes([meta.challenge_node]);
    }

    // console.log(edges_to_highlight.length)
    // edges.clear();
    // edges.update(edges_to_highlight);
}

function updateGraph(self) {
    console.log(self.value);
    let tick = [...timeline[self.value]];

    let edges_dict = {};

    for (const e of edges.get()) {
        edges_dict[e.from.toString() + "-" + e.to.toString()] = e;
    }

    // const edgesView = new vis.DataView(edges);
    /* console.log(tick.length);
    for (const e of tick) {
      let edge = edges_dict[e.from.toString()+"-"+e.to.toString()];
      console.log(edge);
      // edge.value = 1;
    }
    // edgesView
    // edges.update(tmp);
    */
    let edges_to_highlight = [];
    for (let i = 0; i < tick.length; i++) {
        let e = tick[i];
        let edge = edges.get(e.from.toString() + "-" + e.to.toString());

        edges_to_highlight.push({
            id: e.from.toString() + "-" + e.to.toString(),
            from: e.from,
            to: e.to,
            value: e.weight,
            color: "#FF5447",
            background: {
                enabled: true,
                color: "#FF5447",
                size: 1,
                dashes: [20, 10],
            },
        });
    }

    edges.clear();
    edges.update(edges_to_highlight);
}

function slider_button_pressed(slider) {
    if (slider_playing) {
        console.log("stopping slider");
        slider_playing = false;
        clearInterval(slider_mover_id);
        return;
    }
    else {
        console.log("starting slider")
        slider_playing = true;
        slider_mover_id = setInterval(move_slider, 250);
    }
}

function move_slider() {
    if (slider_playing) {
        let slider = document.getElementById('tickslider');
        slider.value++;
        console.log("moved slider to ", slider.value);
        updateGraph(slider);
    }
}



// cookie function copied from stackoverflow
function setCookie(name, value, days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "") + expires + "; path=/";
}

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {
    document.cookie = name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}
