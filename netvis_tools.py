#!/usr/bin/python3

from collections import Counter
import argparse
import json
import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import random
import sys
from trace import Trace
import copy
import attacks

NETVIS_EDGE_DACAY_FACTOR = 1.9  # NEW_WEIGHT = FACTOR^-CONN_AGE
NETVIS_EDGE_DACAY_LIMIT = 0.2  # NEW_WEIGHT = 0 if (WEIGHT < LIMIT)



def jprint(obj):
    print(json.dumps(obj, indent=4))


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


def load_network(path):
    return nx.read_gpickle(path)


def preprocess_trace(network, trace: Trace, out_len=100):
    timeline = list()

    eprint("tracing trace...")

    out_tick_len = trace.nr_rounds / out_len

    # generate all routed paths for all ticks
    traffic = [list() for i in range(trace.nr_rounds)]
    for tick, v in trace.trace.items():
        # things that happened in this tick
        tick_action = list()

        for packet in v['sent'] + v['received']:
            hops = nx.algorithms.shortest_path(
                network,
                source=packet['from'],
                target=packet['to']
            )

            # p = hops[0]
            for a,b in zip(hops, hops[1:]):
                tick_action.append({
                    'from': a,
                    'to': b
                })

        # add the commulated actions to the timeline
        if int(tick) % int(out_tick_len) == 0:
            timeline.append(tick_action)
            # reset the tick_actions
            tick_action = list()


    active_connections = dict()
    out = list()


    # calculate the decays for all paths

    for tick in range(len(timeline)):
        conns = timeline[tick]
        out.append(list())
        dead_conns = list()

        for k, v in active_connections.items():
            # remove connections with weight zero
            if v['weight'] == 0:
                dead_conns.append(k)
                continue

            new_weight = pow(NETVIS_EDGE_DACAY_FACTOR, -(tick - v['born']))
            active_connections[k]['weight'] = 0 if new_weight < NETVIS_EDGE_DACAY_LIMIT else new_weight

        # eprint(f"found {len(dead_conns)} connections")

        for k in dead_conns:
            active_connections.pop(k, None);

        for c in conns:
            active_connections[f"{c['from']}-{c['to']}"] = {
                'from': int(c['from']),
                'to': int(c['to']),
                'born': tick,
                'weight': float(1)
            }

        for k,v in active_connections.items():
            out[tick].append(v)

    for tick in out:
        for conn in tick:
            # print(conn)
            # remove the 'born' entry
            conn.pop('born', None)

    return list(out)


def export_graph_json_old(args):
    graph = load_graph(graph)
    positions = nx.drawing.layout.kamada_kawai_layout(self.graph)
    # positions = nx.random_layout(self.graph)

    # print(self.graph.nodes(data=True))

    # exit(1)

    export_data = {
        'nodes': list(),
        'edges': list(),
        'timeline': list(),
    }


    # export nodes
    for k, v in positions.items():
        # print(self.graph.nodes[k])
        export_data['nodes'].append({
            'id': k, # the id is simply the index of the node
            'x': float(v[0]), # the previously kameda kawai calculated x position of the node
            'y': float(v[1]), # same for the y position
            'weight': {  # the weight is a dict of different values
                            # this makes it easier to choose on the client side
                            # how we want to draw the network
                'degree': self.graph.degree[k],
                'group': random.choice(['router', 'router', 'backbone', 'node', 'node', 'node', 'node'])
                # 'group': 'backbone'
            }
        })

    # export edges
    for f, t in self.graph.edges:
        export_data['edges'].append({
            'from': f,
            'to': t,
        })

    traffic = list()
    # generate some random data over time
    for tick in range(TICKS):
        traffic.append(list())

        for n in range(0, 1):
            source = random.randint(0, self.graph.number_of_nodes()-1)
            target = random.randint(0, self.graph.number_of_nodes()-1)

            hops = nx.algorithms.shortest_path(self.graph, source=source, target=target)

            # p = hops[0]
            for a,b in zip(hops, hops[1:]):
                traffic[tick].append({
                    'from': a,
                    'to': b
                })

    active_connections = dict()
    out = list()
    for tick in range(TICKS):
        conns = traffic[tick]
        out.append(list())
        dead_conns = list()

        for k, v in active_connections.items():
            # remove connections with weight zero
            if v['weight'] == 0:
                dead_conns.append(k)
                continue

            new_weight = pow(NETVIS_EDGE_DACAY_FACTOR, -(tick - v['born']))
            active_connections[k]['weight'] = 0 if new_weight < NETVIS_EDGE_DACAY_LIMIT else new_weight

        eprint(f"found {len(dead_conns)} connections")
        for k in dead_conns:
            active_connections.pop(k, None);

        for c in conns:
            active_connections[f"{c['from']}-{c['to']}"] = {
                'from': int(c['from']),
                'to': int(c['to']),
                'born': tick,
                'weight': float(1)
            }

        for k,v in active_connections.items():
            out[tick].append(v)

    for tick in out:
        for conn in tick:
            # print(conn)
            conn.pop('born', None)

    export_data['timeline'] = list(out)

    jprint(export_data)





def gen_all(args):
    network = load_network(args['network'])
    eprint(args)
    meta_information = {
        'teams': list(),
        'epoch': int(),
    }

    nodes = list();
    eprint("calculating node position")
    positions = nx.drawing.layout.kamada_kawai_layout(network)
    # positions = nx.drawing.layout.random_layout(network)

    # todo: put this into a seperate function
    for k, v in positions.items():
        nodes.append({
            'id': k, # the id is simply the index of the node
            'x': float(v[0]), # the previously kameda kawai calculated x position of the node
            'y': float(v[1]), # same for the y position
            'weight': {}
        })

    for trace_path in args['trace']:
        eprint(f"process trace '{trace_path}'")

        # load trace file
        trace = Trace.from_file(trace_path)
        # use max() here just in case we are loading traces from different epochs
        # ideally we would check if all epochs are the same and otherwise throw an exception
        meta_information['epoch'] = max(int(trace.epoch), meta_information['epoch'])
        meta_information['teams'].append({
            'name': trace.teamID
        })

        trace_nodes = copy.deepcopy(nodes)
        for k, v in trace.sum_node_weights(network).items():
            trace_nodes[k]['weight'] = v
        trace_edges = trace.sum_edges(network)

        attack = attacks.exploit_1(trace)
        attack['comgraph'] = comdict_to_graph(attack['comgraph'])


        team_trace = {
            'meta': {'challenge_node': 550},
            'nodes': trace_nodes,
            'edges': trace_edges,
            'timeline': preprocess_trace(network, trace),
            'attacks': dict(attack),
        }

        # print(attacks.exploit_1(trace))

        with open(f"{args['out_dir']}/teamgraph-{trace.teamID}.json", 'w') as team_out_f:
            team_out_f.write(json.dumps(team_trace))

        # print(team_trace)

    meta_information['available_weights'] = [
        'packets_sent',
        'packets_received',
        'packets_total',
        'bytes_sent',
        'bytes_received',
        'bytes_total',
        'degree'
    ]

    with open(f"{args['out_dir']}/meta.json", 'w') as meta_f:
        meta_f.write(json.dumps(meta_information))



# converts a dict with key=(from, to) value=int
# to a dict with {nodes, edges}

def comdict_to_graph(com: dict) -> dict:
    nodes = list()
    edges = list()


    tmpset = set()
    for k, v in com.items():
        # print(f, t)
        tmpset.add(k[0])
        tmpset.add(k[1])
        edges.append({'from': k[0], 'to': k[1], 'value': v}),

    return {
        'nodes': list(map(lambda x: {'id': x, 'label': str(x)}, tmpset)),
        'edges': edges,
    }








if __name__ == "__main__":
    args_parser = argparse.ArgumentParser(description='Preprocess network visualisations')
    args_parser.add_argument('--network', required=True)
    subcommands = args_parser.add_subparsers(required=True)

    # gen-json
    gen_json_parser = subcommands.add_parser('gen-json')
    gen_json_parser.add_argument('--trace', required=True)
    gen_json_parser.set_defaults(func=export_graph_json_old)

    # gen-json
    # gen_json_parser = subcommands.add_parser('export-nodes')
    # gen_json_parser.add_argument('--trace', required=True)
    # gen_json_parser.set_defaults(func=export_nodes)

    # gen-json-old
    gen_json_parser = subcommands.add_parser('process-trace')
    gen_json_parser.add_argument('--trace', required=True)
    gen_json_parser.set_defaults(func=preprocess_trace)

    # generate all
    gen_json_parser = subcommands.add_parser('gen-all')
    gen_json_parser.add_argument('--out-dir', required=True)
    gen_json_parser.add_argument('--trace', required=True, action='append')
    gen_json_parser.set_defaults(func=gen_all)

    args = args_parser.parse_args()
    # run the command-function
    args.func(vars(args))
