import random
from collections import Counter

import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import json
import sys


TICKS = 120
NETVIS_EDGE_DACAY_FACTOR = 1.9  # NEW_WEIGHT = FACTOR^-CONN_AGE
NETVIS_EDGE_DACAY_LIMIT = 0.2  # NEW_WEIGHT = 0 if (WEIGHT < LIMIT)



class GraphGenerator:
    def __init__(self, nr_nodes, m, p, q):
        self.n = nr_nodes
        self.m = m
        self.p = p
        self.q = q
        self.base_dir = "graph/"
        self.graph_name = "graph_" + str(self.n) + "_" + str(self.m) + "_" + str(int(p * 100)) + "_" + str(int(q * 100))
        self.graph = None
        self.labels = dict()
        for el in range(1000):
            self.labels[el] = el
        self.delays = None
        self.bandwidth = None

    def generate_graph(self, write=True):
        self.graph = nx.generators.extended_barabasi_albert_graph(self.n, self.m, self.p, self.q)
        if write:
            nx.write_gpickle(self.graph, self.base_dir + self.graph_name + ".pickle")

    def load_graph(self):
        try:
            self.graph = nx.read_gpickle(self.base_dir + self.graph_name + ".pickle")
            return True
        except FileNotFoundError:
            return False

    def complete_graph(self, write=True):
        if not self.graph:
            my_graph = nx.read_gpickle(self.base_dir + self.graph_name + ".pickle")
        else:
            my_graph = self.graph
        # Calculate connected components
        graphs = sorted(list(nx.connected_component_subgraphs(my_graph)), key=len)
        lengths = [len(x) for x in graphs]
        largest_component = graphs[-1]
        large_nodes = list(largest_component.nodes())

        # For all others connect a random node of the component to a random node of the largest component
        for g in graphs[:-1]:
            # Connect to random node of largest component
            small_connect = random.choice(list(g.nodes()))
            large_connect = random.choice(large_nodes)
            my_graph.add_edge(small_connect, large_connect)

        assert nx.is_connected(my_graph), "Completion failed"

        self.graph = my_graph
        if write:
            nx.write_gpickle(my_graph, self.base_dir + self.graph_name + ".pickle")

    def show_degree_histogram(self, write=True):
        degree_sequence = sorted([d for n, d in self.graph.degree()], reverse=True)  # degree sequence
        # print "Degree sequence", degree_sequence
        degreeCount = Counter(degree_sequence)
        deg, cnt = zip(*degreeCount.items())

        plt.bar(deg, cnt, width=0.80, color='b')
        plt.title("Degree Histogram")
        plt.ylabel("Count")
        plt.xlabel("Degree")
        if write:
            plt.savefig(self.base_dir + self.graph_name + "_degrees" + ".pdf")
        plt.show()

    def plot_graph(self, write=True, fast=True):
        sizes = [v for v in self.graph.degree]


        sums = 0
        for index, size in sizes:
            sums += size

        sizes = [(y**2 / 2 + y) / 6 for x, y in sizes]

        if fast:
            pos = nx.random_layout(self.graph)
            speed = "fast"
        else:
            pos = nx.kamada_kawai_layout(self.graph)
            speed = "slow"

        nx.drawing.nx_pylab.draw_networkx_nodes(self.graph, pos,
                                                node_size=sizes,
                                                node_color=self.bandwidth,
                                                cmap=plt.cm.Wistia)
        nx.drawing.nx_pylab.draw_networkx_edges(self.graph, pos,
                                                width=0.2)
        nx.drawing.nx_pylab.draw_networkx_labels(self.graph, pos,
                                                 labels=self.labels,
                                                 font_size=1,
                                                 font_color='#242424')
        plt.axis("off")
        if write:
            plt.savefig(self.base_dir + self.graph_name + "_" + speed + ".pdf")
        # plt.show()

    def plot_graph_slow(self, write=True):
        sizes = [v for v in self.graph.degree]

        sums = 0
        for index, size in sizes:
            sums += size

        sizes = [y / 5 for x, y in sizes]

        # Draw the resulting graph
        nx.drawing.nx_pylab.draw_kamada_kawai(
            self.graph,
            with_labels=False,
            node_size=sizes,
            node_color='#8B0000',
            width=0.2,
            edge_cmap=plt.cm.Blues
        )

        if write:
            plt.savefig(self.base_dir + self.graph_name + "_slow" + ".pdf")
        plt.show()

    def compute_delays(self, phi=lambda x: x, write=True):
        if self.graph:
            self.delays = dict(nx.all_pairs_shortest_path_length(self.graph))
            if write:
                matrix = np.zeros([self.n, self.n])
                for i in range(self.n):
                    for j in range(self.n):
                        delay = self.delays[i][j]
                        if delay == 1 or delay == 0:
                            matrix[i][j] = delay
                        else:
                            matrix[i][j] = phi(delay)
                np_matrix = np.array(matrix)
                np.savetxt(self.base_dir + "graph.txt", np_matrix, fmt="%d")

    def compute_bandwidth(self, write=False):
        self.bandwidth = list()

        min_bandwidth = 1000

        centrality = dict(nx.betweenness_centrality(self.graph, normalized=True))
        for i in range(self.n):
            self.bandwidth.append(min_bandwidth + centrality[i] * 50000)

        plt.hist(self.bandwidth, bins=100)
        plt.yscale("log")
        if write:
            plt.savefig(self.base_dir + "betweenness_centrality" + self.graph_name + ".png")
        else:
            plt.show()
        plt.close()

        if write:
            with open(self.base_dir + "bandwidth.txt", mode="w") as file:
                file.write(str(self.n) + "\n")
                for i in range(self.n):
                    file.write(str(round(self.bandwidth[i])) + "\n")

    @staticmethod
    def generate_id(prefix, length, suffix):
        return prefix + "".join(random.choice("0123456789abcdefghijklmnopqrstuvwxyz") for _ in range(length)) + suffix



def main():
    nr_nodes = 1000
    graph_param = 1
    p = 0
    q = 0.15

    G = GraphGenerator(nr_nodes, graph_param, p, q)
    if not G.load_graph():
        G.generate_graph(write=True)
        G.complete_graph(write=True)
    # G.plot_graph(fast=False)
    # G.compute_bandwidth(write=True)
    #G.plot_graph(write=True, fast=False)
    # G.compute_delays(write=True)
    G.export_graph_json()


if __name__ == "__main__":
    main()
