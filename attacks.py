import sys
from trace_utils import TraceReader

def exploit_1(t) -> dict():
    history_data = [{"message_activity": 0, "sender_set_size": 0} for _ in range(t.max_delay)]
    # print(reverse_traces)
    senders = list()
    all_candidates = set()
    all_communications = dict()

    for i in range(t.nr_challenges()):
        challenge_receiver = t.receiver(t.get_challenge(i))
        first_hop_sender = t.sender(t.get_challenge(i))
        possible_senders_set = set()
        possible_senders_set.add(first_hop_sender)
        challenge = t.get_challenge(i)
        last_index = t.received_at(challenge)
        for k,j in enumerate(range(last_index - 1, last_index - t.max_delay - 1, -1)):
            this_ticks_paths = list()
            if str(j) in t.trace:
                for msg in t.trace[str(j)]["received"]:
                    if msg["to"] in possible_senders_set:
                        possible_senders_set.add(msg["from"])
                        history_data[k]["message_activity"] += 1
                        if (msg["from"], msg["to"]) not in all_communications:
                            all_communications[(msg["from"], msg["to"])] = 1
                        all_communications[(msg["from"], msg["to"])] += 1

            history_data[k]['sender_set_size'] = len(possible_senders_set)
        senders.append(possible_senders_set)


    candidates = senders[0]
    for s in senders[1:]:
        all_candidates = all_candidates.union(s)
        candidates.intersection_update(s)

    candidates = list(candidates)

    # print(all_communications)

    return {
        "candidates": candidates,
        "raw_candidates": list(all_candidates),
        "analysis": list(history_data),
        "comgraph": all_communications,
        "challenge_receiver": challenge_receiver
    }



if __name__ == "__main__":
    trace_file = "trace.json"

    if len(sys.argv) == 2:
        trace_file = sys.argv[1]

    trace = TraceReader(filename=trace_file)

    # Call your exploit, that returns a list of candidates (possible flags)
    print(exploit_1(trace))
