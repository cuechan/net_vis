import json
from types import SimpleNamespace
import networkx as nx



class Trace:
    def __init__(self, trace: dict):
        self.epoch: int(self.epoch)
        self.trace = dict()
        self.teamID: str()
        self.success: bool()
        self.nr_rounds: int()
        self.nr_users: int()
        self.max_delay: int()
        self.challenge: list()
        self.__dict__.update(**trace)

    @staticmethod
    def from_file(path: str):
        with open(path) as trace_file:
            data=trace_file.read()
            return Trace(json.loads(data))

    def nr_challenges(self) -> int:
        return len(self.challenge)

    def sender(self, packet_id):
        for _, value in self.trace.items():
            for message in value["sent"]:
                if message["packetID"] == packet_id:
                    return message["from"]

    def receiver(self, packet_id):
        for _, value in self.trace.items():
           for message in value["received"]:
                if message["packetID"] == packet_id:
                    return message["to"]

    def get_challenge(self, i) -> str:
        return self.challenge[i]

    def received_at(self, packet_id):
        for i, value in self.trace.items():
            for message in value["received"]:
                if message["packetID"] == packet_id:
                    return int(i)

    def sum_node_weights(self, network) -> list:
        node_traffic = dict();

        # create empty entries for all nodes
        for n in network.nodes:
            node_traffic[n] = {
                'packets_sent': 0,
                'packets_received': 0,
                'packets_total': 0,
                'bytes_sent': 0,
                'bytes_received': 0,
                'bytes_total': 0,
                'degree': network.degree[n],
            }

        for t, v in self.trace.items():
            # we only need to look at the sent xor received packets.
            # That is because every packet that gets sent by a node,
            # will be received by another. If we wouldnt account for
            # that we would look at each packet twice (from the senders
            # and receivers point of view)
            # we only look at sent packets
            for packet in v['sent']:
                source = packet['from']
                dest = packet['to']

                node_traffic[source]['packets_sent'] += 1
                node_traffic[source]['packets_total'] += 1
                node_traffic[source]['bytes_sent'] += packet['size']
                node_traffic[source]['bytes_total'] += packet['size']

                node_traffic[dest]['packets_received'] += 1
                node_traffic[dest]['packets_total'] += 1
                node_traffic[dest]['bytes_received'] += packet['size']
                node_traffic[dest]['bytes_total'] += packet['size']
        return node_traffic


    def sum_edges(self, network) -> list:
        maximum = 0
        edge_sums = dict()
        all_edges = list()

        for tick, v in self.trace.items():
            for packet in v['received'] + v['sent']:
                # print(packet)
                source = packet['from']
                dest = packet['to']

                hops = nx.algorithms.shortest_path(network, source=source, target=dest)

                # p = hops[0]
                for a,b in zip(hops, hops[1:]):
                    if (a, b) not in edge_sums:
                        edge_sums[(a, b)] = 0;
                    edge_sums[(a, b)] += 1;

                maximum = max(edge_sums[(a, b)], maximum)

        for (a,b),v in edge_sums.items():
            all_edges.append({
                'from': a,
                'to': b,
                'value': min(v/maximum, 1) # make sure we never get over 1
            })

        return all_edges
